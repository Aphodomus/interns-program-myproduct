# Interns Program MyProduct


Projeto feito como o exempo no [Figma](https://bit.ly/31cxgx7), em tema dark, como proposto incialmente. Porém, também foram adicionados o **tema white e o efeito Parallax**, para uma página mais dinâmica. Como solicitado, o projeto tem back-end e front-end feito em Express.js, e foram feitos utilizados:
- Docker;
- Git;
- SASS, ITCSS e RSCSS;
- Web Bundlers;
- HTML Responsivo;
- Express;


# Iniciando o projeto para vizualização

Para executar o projeto, basta rodar no Docker em **localhost:9000/index.html** para a página ínicial product/home.
- Para rodar em docker, execute *docker-compose -p nomedoprojeto up --build*